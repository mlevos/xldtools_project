#!/usr/bin/python
#-*- coding: utf-8 -*-

import base64, os, sys
from os.path import normpath, dirname, join
root = normpath(join(dirname(__file__), '..'))

def get_login():
	login_b64=os.environ.get('LOG_XLD'); 
	if login_b64 == None:
		print "Veuillez charger vos infos utilisteur avec cette commande 'source "+root+"/bin/setup_log'"
		sys.exit(1)
	login = base64.b64decode(login_b64); 
	login = login.replace('\n', '')
	username = login.split(':')[0]
	password = login.split(':')[1:]
	return username, ':'.join(password);
	