import sys
from os.path import normpath, dirname, join
root = normpath(join(dirname(__file__), '.'))
sys.path.insert(0, root)

from get_config import ConfigSectionMap 
from get_login import get_login