﻿#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
	import ConfigParser,sys
	from os.path import normpath, dirname, join
except ImportError: 
	raise ImportError('Il vous manque une ou pluiseurs librairie(s) pour executer le programme')
	

def ConfigSectionMap(file, section):
		'''
			Description:
				Fonction servant a lire les fichier de configuration
			Parametre: 
				file		:str | Chemin du fichier de configuration
				section 	:str | La section concernee dans le fichier de configuration
			
		'''
		#On recupère le vrai lien du fichier configuration
		conf_path = normpath(join(dirname(__file__), '../conf'))
		
		#Création de l'objet de parsin du fichier de configuration
		Config = ConfigParser.ConfigParser() 
		Config.read(conf_path+'/'+file);
		data = {}
		options = Config.options(section)

		#Enregistrement de toute les informations des fichier de configuration
		for option in options:
			try:
				data[option] = Config.get(section, option)
				if data[option] == -1:
					DebugPrint(option+"is missing in"+file_config); 
					sys.exit(2); 
			except:
				print("exception on %s!" % option)
				sys.exit(2); 
		return data