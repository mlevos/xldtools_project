import sys
from os.path import normpath, dirname, join
root = normpath(join(dirname(__file__), '.'))
sys.path.insert(0, root)

from rest import Rest 
from repository_service import RepositoryService
from xldeploy import XLDeploy
from texttable import Texttable, get_color_string, bcolors