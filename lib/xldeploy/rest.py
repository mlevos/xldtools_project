#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
	import urllib2,base64, ssl, sys, urllib
	from StringIO import StringIO
	from poster.encode import multipart_encode
	from poster.streaminghttp import register_openers
except ImportError:
	raise ImportError('Il vous manque une ou pluiseurs librairie(s) pour executer le programme')
		

class Rest:
	'''
		Description : 
			Classe de connexion au serveur XL Deploy, et permettant de faire des requêtes REST
		Paramètres	: 
			auth: str 			| Variable d'authentification mis en base64
			host: str 			| Nom du serveur où se trouve XL Deploy
			port: int 			| Numéro de port du serveur où se trouve XL Deploy
			requestType : str 	| http / https
			
	'''
	def __init__(self, username, password, request_type, host,port):
		self.auth = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
		self.host=host;
		self.port=port;
		self.request_type=request_type;
	
	def request(self, request_method, service_resource, query=None, headers={}):
		'''
			Description	:
				Méthode qui permet d'exécuter un requête REST
			Paramètres	: 
				requestMethod	: str	| Méthode de requête : GET / POST / PUT / DELETE
				service_resource: str	| correspond au service-resource
				query 			: dic 	| dictionnaire comportant tous les query de la requête	
				header 			: dic 	| en-tête de la requête
			Retour		: 
				str 	| 	Retourne la reponse brute renvoyée par le serveur, en cas d'erreur se stop	
				
		'''
		
		#Création d'un contexte pour passer la vérification SSL
		ssl._create_default_https_context = ssl._create_unverified_context	
		#Exécution de l'url
		opener=urllib2.build_opener(urllib2.HTTPHandler);
		url=self.request_type+'://'+self.host+':'+str(self.port)+'/deployit'+service_resource;
		if(query):
			encoded_query=urllib.urlencode(query);
			req = urllib2.Request(url+"?"+encoded_query); 
		else:
			req = urllib2.Request(url)
			
		req.get_method = lambda: request_method;
		req.add_header("Authorization", "Basic %s" % self.auth) ;
		for key,value in headers.iteritems():
			req.add_header(key, value);	
		try:
			response=opener.open(req);
			return response.read();
		except urllib2.HTTPError, e:
			print('Erreur: %s' % e.code); 
			print('url : '+url); 
			print('response :')
			print("Une erreur s'est produite !"); 
			sys.exit(2)
		
	
	def get(self, service_resource, query=None, headers={}):
		'''
			Description	:
				requête GET
			Paramètres	:
				service_resource: str	| correspond au service-resource
				query 			: dic 	| dictionnaire de données 
				headers 		: dic 	| dictionnaire en-tête
			Retour		:
				srt 	: xml de retour
		'''
		return self.request('GET', service_resource, query, headers); 
	
	
	def post(self, service_resource, query=None, headers={}):
		'''
			Description	:
				requête POST
			Paramètres	:
				service_resource: str	| correspond au service-resource
				query 			: dic 	| dictionnaire de données 
				headers 		: dic 	| dictionnaire en-tête
			Retour		:
				srt 			: xml de retour
		'''
		return self.request('POST', service_resource, query, headers);
	
	
	def delete(self, service_resource, query=None, headers={}):
		'''
			Description	:
				requête POST
			Paramètres	:
				service_resource: str	| correspond au service-resource
				query 			: dic 	| dictionnaire de données 
				headers 		: dic 	| dictionnaire en-tête
			Retour		:
				srt 			: xml de retour
		'''
		return self.request('DELETE', service_resource, query, headers);
		
	def post_file(self, service_resource, file):
		register_openers()
		url=self.request_type+'://'+self.host+':'+str(self.port)+'/deployit'+service_resource;
		datagen, headers = multipart_encode({"fileData": open(file)})
		req = urllib2.Request(url ,datagen, headers)
		req.add_header("Authorization", "Basic %s" % self.auth) ;
		req.get_method = lambda: 'POST';
		try:
			response=urllib2.urlopen(req);
			return response.read();
		except urllib2.HTTPError, e:
			print('Code : %s' % e.code); 
			print('url : '+url); 
			print("Error !");
			print e.read()
			sys.exit(2)
		