#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
	from rest import Rest
	import xml.etree.ElementTree as ET
except:
	raise ImportError('Il vous manque une ou pluiseurs librairie(s) dans le fichier repository_service.py ')
	

class Connection:
		def __init__(self, rest):
			self.rest=rest;
			self.service_user = "/security/user/";
		
		def get_user_info(self, username):
			xml = self.rest.get(service_resource=self.service_user+username)
			root = ET.fromstring(xml)
			user = root.find('username');
			return "l'utilisateur "+user.text+" fait bien partie de la liste des utilisateur du systeme", xml