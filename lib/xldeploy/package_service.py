#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
	from rest import Rest
	import xml.etree.ElementTree as ET
except:
	raise ImportError('Il vous manque une ou pluiseurs librairie(s) dans le fichier repository_service.py ')
	
class PackageService: 
		'''
			Description : 
				Comporte toutes la plupart des requ�tes definis � la page : 
				https://docs.xebialabs.com/xl-deploy/5.5.x/rest-api/com.xebialabs.deployit.engine.api.RepositoryService.html
			Param�tres 	: 
				rest : Rest | objet servant aux requ�tes REST 
		'''
		### D�finition des service-resource
		
		
		##	Fin d�finition service-resource
		def __init__(self, rest):
			self.rest=rest;
			self.service_resource_upload = "/package/upload/";
		
		
		
		def upload(self, path_file=''):
			package_name = path_file.split('/')[-1]
			return self.rest.post_file(service_resource=self.service_resource_upload+package_name, file=path_file);
		