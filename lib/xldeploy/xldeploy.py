﻿#!/usr/bin/python
#-*- coding: utf-8 -*-
from rest import Rest
from repository_service import RepositoryService
from package_service import PackageService
from connection import Connection

class XLDeploy:
	'''
		Description	:
			Objet XLDeploy deffinissant toutes les actions possibles avec XL Deploy 
			
		Parametres	:
			rest 				: Rest 					| Objet de requete REST
			repositoy_service 	: RepositoryService		| Objet RepositoryService
			
	'''
	def __init__(self, username, password, request_type, host,port):
		#Initialisation de l'objet Rest
		self.rest=Rest(username, password, request_type, host, port);
		self.repository_service=RepositoryService(self.rest); 
		self.package_service=PackageService(self.rest);
		self.connection=Connection(self.rest)
		
	