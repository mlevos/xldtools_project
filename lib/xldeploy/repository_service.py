#!/usr/bin/python
#-*- coding: utf-8 -*-

try:
	import sys
	from rest import Rest
	import xml.etree.ElementTree as ET
except:
	raise ImportError('Il vous manque une ou pluiseurs librairie(s) dans le fichier repository_service.py ')
	
class RepositoryService: 
		'''
			Description : 
				Comporte toutes la plupart des requ�tes d�finis � la page : 
				https://docs.xebialabs.com/xl-deploy/5.5.x/rest-api/com.xebialabs.deployit.engine.api.RepositoryService.html
			Param�tres 	: 
				rest : Rest | objet servant aux requ�tes REST 
		'''
		### D�finition des service-resource
		
		
		##	Fin d�finition service-resource
		def __init__(self, rest):
			self.rest=rest;
			self.service_resource_query = "/repository/query";
			self.service_resource_repository="/repository/ci/";
			self.service_resource_copy="/repository/copy/";
		
		
		def query(self, type='', namePattern='', resultsPerPage=''):
			'''
				Description	:
					Retourne la liste de CI  rechercher par diff�rents param�tres
				Param�tres	:
					type 		: str 		| Type du CI 
					namePattern	: str 		| Chaine de caract�re repr�sentant partiellement le nom du CI recherch�
				Retour 		: 
					list 		| liste des CI trouv�s
			'''
			query={'type' : type, 'namePattern' : '%'+namePattern+'%', 'resultsPerPage' : resultsPerPage};
			result = list();
			xml = self.rest.get(self.service_resource_query, query);
			root= ET.fromstring(xml); 
			for child in root:
				result.append(child.attrib['ref']);
			return result, xml 
			
		def get_ci_configuration(self, ci):
			'''
				Description	:
					Retourne un xml de type <list> 
				Param�tres	:
					ci		: str			| Nom du CI
				Retour 		: 
					dic 		| dictionnaire ayant les informations sur le CI
					xml			| reponse en format xml
			'''
			result=dict(); 
			xml = self.rest.get(self.service_resource_repository+ci);
			root= ET.fromstring(xml); 
			result['udm'] = root.tag; # r�cup�ration du type UDM
			for child in root.iter(root.tag): 
				for key, value in child.attrib.iteritems():
					result[key] = value; 
			return result, xml;
			
		def copy(self, ci, newId):
			'''
				Description	:
					Retourne un xml de type <list> 
				Param�tres	:
					ci		: str			| Nom du CI
				Retour 		: 
					dic 		| dictionnaire ayant les informations sur le CI copi� 
					xml			| reponse en format xml
			'''
			result=dict();
			query={'newId' : newId};
			headers={'Content-type':  'application/xml'};
			xml = self.rest.post(service_resource=self.service_resource_copy+ci,query=query, headers=headers);
			root= ET.fromstring(xml); 
			result['udm'] = root.tag; # r�cup�ration du type UDM
			for child in root.iter(root.tag): 
				for key, value in child.attrib.iteritems():
					result[key] = value; 
			return result, xml;
			
		def delete(self, ci):
			'''
				Description	:
					Retourne un xml de type <list> 
				Param�tres	:
					ci		: str			| Nom du CI
				Retour 		: 
					dic 		| dictionnaire ayant les informations sur le CI
					xml			| reponse en format xml
			'''
			result=dict();
			result = self.rest.delete(service_resource=self.service_resource_repository+ci);
			return result;
		
		def get_deployables(self, version):
			'''
				Description	:
					Retourne un xml de type <list> 
				Param�tres	:
					ci		: str			| CI udm.Version 
				Retour 		: 
					list 		| liste des deployables de l'application
					xml			| reponse en format xml
			'''
			deployables_liste = []
			xml = self.get_ci_configuration(version)[1]
			udm_application = ET.fromstring(xml)
			if(udm_application.tag != 'udm.DeploymentPackage'):
				print "Le ci que vous avez rentrer n'est pas correcte";
				sys.exit(1)
			deployables = udm_application.find('deployables'); 
			for ci in deployables:
				deployables_liste.append(ci.attrib['ref'])
			return deployables_liste;
		