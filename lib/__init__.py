import sys, os
from os.path import normpath, dirname, join
root = normpath(join(dirname(__file__), '.'))
sys.path.insert(0, root)

from xldeploy import XLDeploy